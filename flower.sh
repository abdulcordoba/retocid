#!/bin/bash

sudo docker run \
    -d \
    -p 8003:8000 \
    -v $PWD/site:/site \
    --name="celery-flower" \
    iadb/retocid:celery \
    /usr/bin/python /site/retocid/manage.py celery flower --port=8000
