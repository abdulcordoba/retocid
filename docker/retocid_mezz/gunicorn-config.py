from __future__ import unicode_literals
import multiprocessing

bind = "unix:/site/retocid_mezz/gunicorn.sock"
workers = 2
errorlog = "/site/retocid_mezz/logs/retocid_error.log"
loglevel = "error"
proc_name = "retocid"
