"""
Django settings for retocid project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
from django.templatetags.static import static
from django.utils.six.moves import configparser
config = configparser.SafeConfigParser(allow_no_value=True)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
config.read('%s/local.cfg' % (BASE_DIR))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config.get('general', 'SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config.get('general', 'DEBUG') == 'True'

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['retocid.info']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'fitapp',
    'myauth',
    'bootstrap3',
    'reports',
    'djcelery',
)

if DEBUG:
    INSTALLED_APPS += ('debug_toolbar', 
        'django_extensions',
    )

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',    # This must be first on the list
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware', # This must be last
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
)

ROOT_URLCONF = 'retocid.urls'

WSGI_APPLICATION = 'retocid.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': config.get('db', 'NAME'),
        'USER': config.get('db', 'USER'),
        'PASSWORD': config.get('db', 'PASS'),
        'HOST': config.get('db', 'HOST'),
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

if DEBUG:
    STATIC_ROOT = '/site/static_retocid'
    STATIC_URL = '/static/'
else:
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    AWS_STORAGE_BUCKET_NAME = config.get('aws', 'AWS_BUCKET')
    STATIC_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
    AWS_QUERYSTRING_AUTH = False

#INSTALLED_APPS += ('storages',)

AWS_ACCESS_KEY_ID=config.get('aws', 'AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY=config.get('aws', 'AWS_SECRET_ACCESS_KEY')

from datetime import datetime, timedelta
expires = datetime.utcnow() + timedelta(days=(365))
expires = expires.strftime("%a, %d %b %Y %H:%M:%S GMT")
AWS_HEADERS = {
    'Cache-Control': 'max-age=86400',
    'Expires': expires, 
}

FITAPP_CONSUMER_KEY=config.get('fitbit', 'FITAPP_CONSUMER_KEY')
FITAPP_CONSUMER_SECRET=config.get('fitbit', 'FITAPP_CONSUMER_SECRET')
AUTHENTICATION_BACKENDS=('retocid.auth_backend.MyBackend', )

BOOTSTRAP3 = {
    'theme_url': STATIC_URL + 'signin.css',
    'javascript_in_head': True,
}

if DEBUG:
    CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            }
    }
else:
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': config.get('redis', 'HOST'),
            'TIMEOUT' : 3600,
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            }
        }
    }


BROKER_URL = config.get('redis', 'HOST')
CELERY_RESULT_BACKEND = config.get('redis', 'HOST')
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']

djcelery.setup_loader()

CACHE_MIDDLEWARE_ALIAS='default'
CACHE_MIDDLEWARE_SECONDS=60*15
CACHE_MIDDLEWARE_KEY_PREFIX=''

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK' : 'retocid.celery.ftrue'
}

if DEBUG:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },

        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            },
         'console':{
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            'django.request': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }
