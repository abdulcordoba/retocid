from django.contrib import admin
from reports.models import Version, DailyEntry, MyUserFitbit

# Register your models here.

@admin.register(Version)
class VersionAdmin(admin.ModelAdmin):
    list_display = ('descripcion', 'active')
    ordering = ('id',)
    filter_horizontal = ('users',)

@admin.register(DailyEntry)
class DailyEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('version', 'source', 'date', 'user', 'steps')
    ordering = ('date',)
    list_filter = ('version', 'source', 'user')

@admin.register(MyUserFitbit)
class MyUserFitbitAdmin(admin.ModelAdmin):
    pass
