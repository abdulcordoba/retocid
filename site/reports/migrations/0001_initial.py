# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitapp', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('steps', models.IntegerField()),
                ('user', models.ForeignKey(to='fitapp.UserFitbit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
