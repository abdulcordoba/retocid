# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitapp', '0001_initial'),
        ('reports', '0006_version_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='users',
            field=models.ManyToManyField(to='fitapp.UserFitbit'),
            preserve_default=True,
        ),
    ]
