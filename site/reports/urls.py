from django.conf.urls import patterns, include, url
from reports import views

urlpatterns = patterns('',
    url(r'^steps/$', views.steps, name='steps'),
    url(r'^g/(?P<anio>\d+)/(?P<mes>\d+)/(?P<dia>\d+)/(?P<n>\d+)/(?P<version>\d+)/promedios/$', views.promedios, name='promedios'),
    url(r'^promedio/total/$', views.promedio_total, name='promedio_total'),
    url(r'^participantes/$', views.participantes, name='participantes'),
    url(r'^comparacion/$', views.comparacion, name='comparacion'),
    url(r'^cdf/$', views.cdf, name='cdf'),
)
