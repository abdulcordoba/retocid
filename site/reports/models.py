from django.db import models
from fitapp.models import UserFitbit
from django.contrib.auth.models import User, Group

# Create your models here.

TRACKER = 0
TOTAL = 1

class MyUserFitbit(UserFitbit):
    authorized = models.BooleanField(default=False)

class DailyEntry(models.Model):
    user = models.ForeignKey(UserFitbit)
    date = models.DateField()
    source = models.IntegerField()
    steps = models.IntegerField()
    version = models.ForeignKey('Version')
    def __str__(self):              # __unicode__ on Python 2
        return self.date

    class Meta:
        unique_together = ("user", "date", "source", "version")
        index_together = [ ['user', 'date', 'source', 'version'] ]

class Version(models.Model):
    descripcion = models.CharField(max_length=64)
    active = models.BooleanField(default=False)
    users = models.ManyToManyField(User)
    def __str__(self):              # __unicode__ on Python 2
        return self.descripcion
